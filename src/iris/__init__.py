from sklearn import datasets
import numpy
from matplotlib import pyplot
from sklearn.preprocessing.data import StandardScaler
iris = datasets.load_iris()
X_iris, y_iris = iris.data, iris.target
print X_iris.shape, y_iris.shape
print X_iris[0], y_iris[0]


from sklearn.cross_validation import train_test_split
from sklearn import preprocessing
X, y = X_iris[:, :2], y_iris
X_train, X_test, y_train, y_test = train_test_split(X, y,test_size=0.25, random_state=33)

print X_train.shape, y_train.shape

scaler = preprocessing.StandardScaler().fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)


import matplotlib.pyplot as plt

colors = ['red', 'greenyellow', 'blue']

for i in xrange(len(colors)):
    xs = X_train[:, 0][y_train == i]
    ys = X_train[:, 1][y_train == i]
    plt.scatter(xs, ys, c=colors[i])

plt.legend(iris.target_names)
plt.xlabel('Sepal length')
plt.ylabel('Sepal width')
plt.ion()
plt.show()






    
from sklearn.linear_model import SGDClassifier
clf = SGDClassifier()
clf.fit(X_train, y_train)

print clf.coef_
print clf.intercept_


# Drawing three decision boundaries
"""
x_min, x_max = X_train[:, 0].min() - .5, X_train[:, 0].max() + .5
y_min, y_max = X_train[:, 1].min() - .5, X_train[:, 1].max() + .5
xs = numpy.arange(x_min, x_max, 0.5)
fig, axes = plt.subplots(1, 3)
fig.set_size_inches(10, 6)
for i in [0,1,2]:
    axes[i].set_aspect('equal')
    axes[i].set_title('Class '+ str(i) + ' versus the rest')
    axes[i].set_xlabel('Sepal length')
    axes[i].set_ylabel('Sepal width')
    axes[i].set_xlim(x_min, x_max)
    axes[i].set_ylim(y_min, y_max)
    sca(axes[i])
    plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train,cmap=plt.cm.prism)
    
    
"""    
print clf.predict(scaler.transform([4.7, 3.1]))
print clf.decision_function(scaler.transform([[4.7, 3.1]]))


#Now predicting the accuracy of the training set
from sklearn import metrics
y_train_pred = clf.predict(X_train)
print metrics.accuracy_score(y_train, y_train_pred) 


y_pred = clf.predict(X_test)
print metrics.accuracy_score(y_test, y_pred)


print metrics.classification_report(y_test, y_pred ,target_names=iris.target_names)

#Shows what type of errors the classifier is making
print metrics.confusion_matrix(y_test, y_pred)


"""Composite estimator made by a pipeline of (standardization and linear models)."""
from sklearn.cross_validation import cross_val_score, KFold
from sklearn.pipeline import Pipeline
clf = Pipeline([('scaler', StandardScaler()),('linear_model',SGDClassifier())])
#Creating a k-fold cross validation iterator of k=5 folds
cv = KFold(X.shape[0], 5, shuffle=True, random_state=33)
scores = cross_val_score(clf, X, y, cv=cv)
print scores

from scipy.stats import sem
print "numpy" , numpy

def mean_score(scores):
    return ("Mean score: {0:.3f} (+/-{1:.3f})").format(numpy.mean(scores), sem(scores))

print mean_score(scores)

