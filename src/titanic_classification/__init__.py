#Classify if person survived or not
import csv
import numpy as np


with open('titanic.csv', 'rb') as csvfile:
    titanic_reader = csv.reader(csvfile, delimiter=',',quotechar='"')
    # Header contains feature names
    row = titanic_reader.next()
    feature_names = np.array(row)
    # Load dataset, and target classes
    titanic_X, titanic_y = [], []
    for row in titanic_reader:
        titanic_X.append(row)
        titanic_y.append(row[2]) # The target value issurvived"
    titanic_X = np.array(titanic_X)
    titanic_y = np.array(titanic_y)

print feature_names
print titanic_X[0], titanic_y[0]

#we keep class, age, sex
titanic_X = titanic_X[: ,[1,4,10]]
feature_names = feature_names[[1, 4, 10]]

ages = titanic_X[:, 1]
mean_age = np.mean(titanic_X[ages != 'NA',1].astype(np.float))
titanic_X[titanic_X[:, 1] == 'NA', 1] = mean_age

from sklearn.preprocessing import LabelEncoder
enc = LabelEncoder()

label_encoder = enc.fit(titanic_X[:, 2])
integer_classes =label_encoder.transform(label_encoder.classes_)
print "Integer classes:", integer_classes

t = label_encoder.transform(titanic_X[:, 2])
titanic_X[:, 2] = t

print feature_names
print titanic_X[12], titanic_y[12]


from sklearn.preprocessing import OneHotEncoder

enc = LabelEncoder()
label_encoder = enc.fit(titanic_X[:, 0])

from sklearn.preprocessing import OneHotEncoder
enc = LabelEncoder()
label_encoder = enc.fit(titanic_X[:, 0])
integer_classes =label_encoder.transform(label_encoder.classes_).reshape(3, 1)

#Onehotencoder is used to split a parameter which has three
   
enc = OneHotEncoder()
one_hot_encoder = enc.fit(integer_classes)
num_of_rows = titanic_X.shape[0]
t = label_encoder.transform(titanic_X[:,0]).reshape(num_of_rows, 1)
new_features = one_hot_encoder.transform(t)
titanic_X = np.concatenate([titanic_X,new_features.toarray()], axis = 1)
titanic_X = np.delete(titanic_X, [0], 1)
feature_names = ['age', 'sex', 'first_class', 'second_class','third_class']
titanic_X = titanic_X.astype(float)
titanic_y = titanic_y.astype(float)

print feature_names

from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(titanic_X, titanic_y, test_size=0.25, random_state=33)
from sklearn import tree
clf = tree.DecisionTreeClassifier(criterion='entropy',max_depth=3,min_samples_leaf=5)
clf = clf.fit(X_train,y_train)


import pydot, StringIO
dot_data = StringIO.StringIO()
tree.export_graphviz(clf, out_file=dot_data,feature_names=['age','sex','1st_class','2nd_class','3rd_class'])
graph = pydot.graph_from_dot_data(dot_data.getvalue())
graph[0].write_png('titanic.png')
from IPython.core.display import Image
Image(filename='titanic.png')


from sklearn import metrics
def measure_performance(X,y,clf, show_accuracy=True,show_classification_report=True, show_confusion_matrix=True):
    y_pred=clf.predict(X)
    if show_accuracy:
        print "Accuracy:{0:.3f}".format(metrics.accuracy_score(y, y_pred)),"\n"
    if show_classification_report:
        print "Classification report"
        print metrics.classification_report(y,y_pred),"\n"
    if show_confusion_matrix:
        print "Confussion matrix"
        print metrics.confusion_matrix(y,y_pred),"\n"

measure_performance(X_train,y_train,clf,True,show_classification_report=False, show_confusion_matrix=False)

#Evaluating performance
clf_dt = tree.DecisionTreeClassifier(criterion='entropy', max_depth=3, min_samples_leaf=5)
clf_dt.fit(X_train, y_train)
measure_performance(X_test, y_test, clf_dt)



