#===============================================================================
# Parallel Grid
#===============================================================================

from sklearn.externals import joblib
from sklearn.cross_validation import ShuffleSplit
import os
def persist_cv_splits(X, y, K=3, name='data',suffix="_cv_%03d.pkl"):"""Dump K folds to filesystem."""
    cv_split_filenames = []
    # create KFold cross validation
    cv = KFold(n_samples, K, shuffle=True, random_state=0)
    # iterate over the K folds
    for i, (train, test) in enumerate(cv):
        cv_fold = ([X[k] for k in train], y[train], [X[k] fork in test], y[test])
        cv_split_filename = name + suffix % i
        cv_split_filename = os.path.abspath(cv_split_filename)
        joblib.dump(cv_fold, cv_split_filename)
        cv_split_filenames.append(cv_split_filename)
    return cv_split_filenames

cv_filenames = persist_cv_splits(X, y, name='news')





def compute_evaluation(cv_split_filename, clf, params):
    # All module imports should be executed in the worker namespace
    
    from sklearn.externals import joblib
    # load the fold training and testing partitions from the filesystem
    X_train, y_train, X_test, y_test = joblib.load(cv_split_filename, mmap_mode='c')
    clf.set_params(**params)
    clf.fit(X_train, y_train)
    test_score = clf.score(X_test, y_test)
    return test_score




def parallel_grid_search(lb_view, clf, cv_split_filenames, param_grid):
    all_tasks = []
    all_parameters = list(IterGrid(param_grid))
    # iterate over parameter combinations
    for i, params in enumerate(all_parameters):
        task_for_params = []
        # iterate over the K folds
        for j, cv_split_filename in   enumerate(cv_split_filenames):
            t = lb_view.apply(compute_evaluation, cv_split_filename, clf,params)
            task_for_params.append(t)
            all_tasks.append(task_for_params)

    return all_parameters, all_tasks
    
    
    
from sklearn.svm import SVC
from IPython.parallel import Client
client = Client()
lb_view = client.load_balanced_view()
all_parameters, all_tasks = parallel_grid_search(lb_view, clf, cv_filenames, parameters)


def print_progress(tasks):
    progress = np.mean([task.ready() for task_group in tasks for task in task_group])
    print "Tasks completed: {0}%".format(100 * progress)
    
print_progress(all_tasks)



def find_bests(all_parameters, all_tasks, n_top=5):
    """Compute the mean score of the completed tasks"""
    mean_scores = []
    for param, task_group in zip(all_parameters, all_tasks):
        scores = [t.get() for t in task_group if t.ready()]
        if len(scores) == 0:
            continue
        mean_scores.append((np.mean(scores), param))
    return sorted(mean_scores, reverse=True)[:n_top]

print find_bests(all_parameters, all_tasks)
