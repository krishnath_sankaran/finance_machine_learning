#To search for best combination of parameters for a model

from sklearn.grid_search import GridSearchCV
from sklearn.pipeline import Pipeline
import numpy as np

parameters = {
'svc__gamma': np.logspace(-2, 1, 4),
'svc__C': np.logspace(-1, 1, 3),
}
clf = Pipeline([('vect', TfidfVectorizer(stop_words=stop_words,token_pattern=ur"\b[a-z0-9_\-\.]+[a-z][a-z0-9_\-\.]+\b",)),('svc', SVC()),])
gs = GridSearchCV(clf, parameters, verbose=2, refit=False, cv=3)

%time _ = gs.fit(X_train, y_train)
gs.best_params_, gs.best_score_
