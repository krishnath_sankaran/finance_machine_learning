'''
Created on Jan 10, 2017

@author: ib121
'''
import datetime
import numpy as np
import pandas as pd
import sklearn
import pandas_datareader.data as web


from sklearn.linear_model import  LogisticRegression
from sklearn.lda import  LDA
from sklearn import qda
from __builtin__ import str




if __name__ == '__main__':
    pass

def fit_model(name, model, x_train, y_train, x_test, pred):
    """Fits a classification model (for our purposes this is LR, LDA and QDA)
    using the training data, then makes a prediction and subsequent hit rat
    for the test data."""
    
    #Fit  and predict the model on the training and then test data
#     print "before X train"
#     print x_train.to_string()
#     print "before Y train"
#     print y_train.to_string()
    
    model.fit(x_train, y_train)
    pred[name] = model.predict(x_test)
    
    #Calculate the hit rate based on the direction tested
    pred["%s_Correct" % name] = (1.0 + pred[name]*pred["Actual"])/2.0
    hit_rate = np.mean(pred["%s_Correct" % name])
    print "%s: %.3f" % (name, hit_rate)    
    
    
    
    
    
    
def create_lagged_series(symbol, start_date, end_date, lags=5):
    """This creates a pandas DataFrame that stores the percentage returns of the 
    adjusted closing value of a stock obtained from Yahoo Finance, along with 
    a number of lagged returns from the prior trading days (lags defaults to 5 days).
    Trading volume, as well as the Direction from the previous day, are also included."""
    
    
    #Obtain stock information from Yahoo finance
    ts = web.DataReader(symbol, "yahoo", start_date-datetime.timedelta(days=365), end_date)
    
    #Create the new lagged Dataframe
    tslag = pd.DataFrame(index=ts.index)
    tslag["Today"] = ts["Adj Close"]
    tslag["Volume"] = ts["Volume"]
    
    #Now creating the colums that show the shifted lag
    for i in xrange(0, lags):
        tslag["Lag%s" % str(i+1)] = ts["Adj Close"].shift(i+1)
    
     
    
    #return frame that shows the percentage change
    tsret = pd.DataFrame(index=tslag.index)
    tsret["Volume"] = tslag["Volume"]
    tsret["Today"] = tslag["Today"].pct_change()*100.0
    tsret["Today"][0] =  0
    
    
    
    #Cleaning the Dataframe by removing zeros
    for i,x in enumerate(tsret["Today"]):
        if(abs(x) < 0.0001):
            tsret["Today"][i] = 0.0001
            
            
    #Create the lagged percentage returns 
    for i in xrange(0, lags):
        tsret["Lag%s" % str(i+1)] = tslag["Lag%s" % str(i+1)].pct_change() * 100.0
    
    #Create the "Direction" column (+1 or -1) indicating up/down day
    
    
    tsret["Direction"] = np.sign(tsret["Today"])
    tsret = tsret[tsret.index >= start_date]
    
    return tsret

