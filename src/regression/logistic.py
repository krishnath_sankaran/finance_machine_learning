'''
Created on Jan 10, 2017

@author: ib121
'''
from regression.utility import create_lagged_series, fit_model
import  datetime
import pandas as pd
from sklearn.linear_model.logistic import LogisticRegression
from sklearn.lda import LDA
from sklearn.qda import QDA

if __name__ == '__main__':
    #Create a lagged series of the S&P500 stock market index
    snpret = create_lagged_series("GSPC", datetime.datetime(2001,1,10), datetime.datetime(2005,12,31), lags=5)
#     print "before snpret"
#     print snpret
    #Use the prior two days of returns as predictor values
    X = snpret[["Lag1", "Lag2"]]
    Y = snpret["Direction"]
    
    #start_test = pd.to_datetime('1/1/2005')
    
    start_test = datetime.datetime(2005,11,23)
        
#     print "X is"
    X = X.fillna(0.0)
    
    
    #Create training and test sets
    X_train = X[X.index < start_test]
#     print "X_train is"
#     print X_train.to_string()
    X_test = X[X.index >= start_test]
    Y_train = Y[Y.index < start_test]
#     print "Y_train is"
#     print Y_train.to_string()
    Y_test = Y[Y.index >= start_test]
    
    
    #Create prediction DataFrame
    pred = pd.DataFrame(index=Y_test.index)
    pred["Actual"] = Y_test
    
    
    #Create and fit the models
    print "Hit rates"
    models = [("LR", LogisticRegression()), ("LDA", LDA()), ("QDA", QDA())]
    for m in models:
        fit_model(m[0], m[1] , X_train, Y_train, X_test, pred)
    
    
    