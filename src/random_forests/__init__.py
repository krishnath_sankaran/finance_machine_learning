#Algorithms that use multiple classifiers to arrive at a solution are Ensemble methods
#Each feature set is split into multiple sets that are used to create decision trees, the class suggested by multiple trees is 
#elected as successful
import titanic_classification

from sklearn.ensemble import RandomForestClassifier
clf = RandomForestClassifier(n_estimators=10, random_state=33)
clf = clf.fit(X_train, y_train)
loo_cv(X_train, y_train, clf) 