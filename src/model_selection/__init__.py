#===============================================================================
# Model selection techniques
#===============================================================================
import matplotlib.pyplot as plt 
from sklearn.datasets import fetch_20newsgroups
news = fetch_20newsgroups(subset='all')
n_samples = 3000
X_train = news.data[:n_samples]
y_train = news.target[:n_samples]


from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer


def get_stop_words():
    result = set()
    for line in open('stopwords_en.txt', 'r').readlines():
        result.add(line.strip())
    return result


stop_words = get_stop_words()
clf = Pipeline([('vect', TfidfVectorizer(stop_words=stop_words,token_pattern=ur"\b[a-z0-9_\-\.]+[a-z][a-z0-9_\-\.]+\b",)),('nb', MultinomialNB(alpha=0.01)),])

from sklearn.cross_validation import cross_val_score, KFold
from scipy.stats import sem
def evaluate_cross_validation(clf, X, y, K):
    # create a k-fold croos validation iterator of k=5 folds
    cv = KFold(len(y), K, shuffle=True, random_state=0)
    # by default the score used is the one returned by score method of the estimator (accuracy)
    scores = cross_val_score(clf, X, y, cv=cv)
    print scores
    print ("Mean score: {0:.3f} (+/-{1:.3f})").format(np.mean(scores), sem(scores))


evaluate_cross_validation(clf, X_train, y_train, 3)



def calc_params(X, y, clf, param_values, param_name, K):
    # initialize training and testing scores with zeros
    train_scores = np.zeros(len(param_values))
    test_scores = np.zeros(len(param_values))
    # iterate over the different parameter values
    for i, param_value in enumerate(param_values):
        print param_name, ' = ', param_value
        
        clf.set_params(**{param_name :param_value})
        # initialize the K scores obtained for each fold
        k_train_scores = np.zeros(K)
        k_test_scores = np.zeros(K)
        # create KFold cross validation
        cv = KFold(n_samples, K, shuffle=True, random_state=0)
        # iterate over the K folds
        for j, (train, test) in enumerate(cv):
            clf.fit([X[k] for k in train], y[train])
            k_train_scores[j] = clf.score([X[k] for k in train], y[train])
            k_test_scores[j] = clf.score([X[k] for k in test], y[test])
            
            
    plt.semilogx(param_values, train_scores, alpha=0.4, lw=2, c='b')
    plt.semilogx(param_values, test_scores, alpha=0.4, lw=2,c='g')
    plt.xlabel("Alpha values")
    plt.ylabel("Mean cross-validation accuracy")
    return train_scores, test_scores

alphas = np.logspace(-7, 0, 8)
print alphas


train_scores, test_scores = calc_params(X_train, y_train, clf,alphas, 'nb__alpha', 3)


print 'training scores: ', train_scores


