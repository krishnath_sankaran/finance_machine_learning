#kmeans splits the data into clusters 
import numpy as np
import matplotlib.pyplot as plt

from sklearn.datasets import load_digits
from sklearn.preprocessing import scale
digits = load_digits()
data = scale(digits.data)

def print_digits(images,y,max_n=10):
    
    # set up the figure size in inches
    fig = plt.figure(figsize=(12, 12))
    fig.subplots_adjust(left=0, right=1, bottom=0, top=1,hspace=0.05, wspace=0.05)
    i = 0

    while i <max_n and i <images.shape[0]:
        # plot the images in a matrix of 20x20
        p = fig.add_subplot(20, 20, i + 1, xticks=[],yticks=[])
        p.imshow(images[i], cmap=plt.cm.bone)
        # label the image with the target value
        p.text(0, 14, str(y[i]))
        i = i + 1
        fig.savefig(str(i))
        
print_digits(digits.images, digits.target, max_n=10)

from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test, images_train,images_test = train_test_split(data, digits.target, digits.images, test_size=0.25,random_state=42)
n_samples, n_features = X_train.shape
n_digits = len(np.unique(y_train))
labels = y_train

from sklearn import cluster
clf = cluster.KMeans(init='k-means++',n_clusters=10, random_state=42)
print X_train
clf.fit(X_train)
print_digits(images_train, clf.labels_, max_n=10)
y_pred=clf.predict(X_test)

def print_cluster(images, y_pred, cluster_number):
    images = images[y_pred==cluster_number]
    y_pred = y_pred[y_pred==cluster_number]
    print_digits(images, y_pred,max_n=10)

for i in range(10):
    print_cluster(images_test, y_pred, i)

#Performance measurement for clustering is done through adjusted rand index
from sklearn import metrics
print "Adjusted rand score:{:.2}".format(metrics.adjusted_rand_score(y_test, y_pred))
print metrics.confusion_matrix(y_test, y_pred)

from sklearn import decomposition
pca = decomposition.PCA(n_components=2).fit(X_train)
reduced_X_train = pca.transform(X_train)
h = .01
x_min, x_max = reduced_X_train[:, 0].min() + 1,reduced_X_train[:, 0].max() - 1
y_min, y_max = reduced_X_train[:, 1].min() + 1,reduced_X_train[:, 1].max() - 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h),np.arange(y_min, y_max, h))
kmeans = cluster.KMeans(init='k-means++', n_clusters=n_digits,n_init=10)
kmeans.fit(reduced_X_train)
Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])
Z = Z.reshape(xx.shape)
plt.figure(1)
plt.clf()
plt.imshow(Z, interpolation='nearest',extent=(xx.min(), xx.max(), yy.min(),
yy.max()), cmap=plt.cm.Paired, aspect='auto', origin='lower')
plt.plot(reduced_X_train[:, 0], reduced_X_train[:, 1], 'k.',
markersize=2)

centroids = kmeans.cluster_centers_
plt.scatter(centroids[:, 0], centroids[:, 1],marker='.',
s=169, linewidths=3, color='w', zorder=10)
plt.title('K-means clustering on the digits dataset (PCAreduced data)\nCentroids are marked with white dots')
plt.xlim(x_min, x_max)
plt.ylim(y_min, y_max)
plt.xticks(())
plt.yticks(())
plt.show()
