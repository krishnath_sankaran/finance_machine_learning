'''
Created on Jan 11, 2017

@author: kichu
'''
import urllib2
import json

if __name__ == '__main__':
    pass


def construct_future_symbols(symbol, start_year=2010, end_year=2014):
    """Constructs a list of futures contract codes for a particular symbol and timeframe"""
    futures = []
    months = 'HMUZ'
    for y in range(start_year, end_year+1):
        for m in months:
            futures.append("%s%s%s" % (symbol, m , y))
    print "futures is", str(futures)
    return futures


def download_contract_from_quandl(contract, auth_token, dl_dir):
    """Download and store future price information from Quandl and store it in a dir"""
    
    #api_url = "https://www.quandl.com/api/v3/datasets/CME/WH1960.csv?api_key=Hz3zHyCzx3KqLd6S6mYb"
    #api_url = "http://www.quandl.com/api/v3/datasets/CME/WH1960.csv"
    api_url = "http://www.quandl.com/api/v3/datasets/CME/%s.csv" % contract
    params = "?api_key=%s&sort_order=asc" % auth_token
    
    with open('config.json') as config_file:
        proxy_setting = json.load(config_file)["proxy_setting"]
        print proxy_setting
        
        
    #setting proxy
    proxies = urllib2.ProxyHandler(json.loads(proxy_setting))
    opener = urllib2.build_opener(proxies)
    urllib2.install_opener(opener)
    
    
    data  = urllib2.urlopen("%s%s" % (api_url, params)).read()
    
    fc = open('%s/%s.csv' % (dl_dir, contract), 'w')
    fc.write(data)
    fc.close()
    
def download_historical_contracts(symbol, auth_token, dl_dir, start_year=2010, end_year=2014):
    contracts = construct_future_symbols(symbol, start_year, end_year)
    for c in contracts:
        download_contract_from_quandl(c, auth_token, dl_dir)
        
            