from affinity_propogation import *

from sklearn import mixture
gm = mixture.GMM(n_components=9,covariance_type='tied', random_state=42)
gm.fit(X_train)

y_pred = gm.predict(X_test)
print "Adjusted randscore:{:.2}".format(metrics.adjusted_rand_score(y_test,y_pred))
print "Homogeneity score:{:.2}".format(metrics.homogeneity_score(y_test, y_pred))
print "Completeness score: {:.2}".format(metrics.completeness_score(y_test, y_pred))

